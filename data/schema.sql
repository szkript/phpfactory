DROP TABLE product;
CREATE TABLE product (id INTEGER PRIMARY KEY AUTOINCREMENT, product_name varchar(100) NOT NULL, price int(100) NOT NULL);
INSERT INTO product (product_name, price) VALUES ('The Military Wives', 2111);
INSERT INTO product (product_name, price) VALUES ('Adele', 333232);
INSERT INTO product (product_name, price) VALUES ('Bruce Springsteen', 12122);
INSERT INTO product (product_name, price) VALUES ('Lana Del Rey', 001011);
INSERT INTO product (product_name, price) VALUES ('Gotye', 3323222323);